#version 450

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_tex_coords;
layout(location = 3) in mat4 v_modelview_mat;

layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform texture2D t_diffuse;
layout(set = 0, binding = 1) uniform sampler s_diffuse;

layout(set = 1, binding = 0)
uniform Uniforms {
    vec3 u_view_position;
    mat4 u_view_proj;
};

struct Light {
    vec3 light_position;
    vec3 light_color;
};

#define NUM_LIGHTS  5
layout(set = 2, binding = 0) uniform Lights {
    Light lights[NUM_LIGHTS];
};

void main() {
    vec4 object_color = texture(sampler2D(t_diffuse, s_diffuse), v_tex_coords);
    
    vec3 view_dir = normalize(u_view_position - v_position);
    
    float spec_pwr = 32.0;
    vec3 color = vec3(0.0, 0.0, 0.0);
    
    for(int i = 0; i < NUM_LIGHTS; ++i) {
        vec3 light_pos = (v_modelview_mat*vec4(lights[i].light_position, 0.0)).xyz;
        vec3 light_dir = normalize(light_pos - v_position);
        
        color += lights[i].light_color*max(dot(v_normal, light_dir), 0.0);
        
        vec3 half_dir = normalize(view_dir + light_dir);
        color += lights[i].light_color*pow(max(dot(v_normal, half_dir), 0.0), spec_pwr);
    }
    
    f_color = vec4(color*object_color.xyz, object_color.a);
}