// Vertex-colored lines

use wgpu::util::DeviceExt;
use glam::{Mat4};

use crate::wgpu_scene::Scene;
use crate::wgpu_renderer::VBData;
use crate::wgpu_scene::InstanceHandle;
use crate::wgpu_scene::InstanceID;
use crate::wgpu_scene::ObjInstance;


#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct LinesVertex {
    pub position: [f32; 3],
    pub color: [u8; 4],
}

impl VBData for LinesVertex {
    fn layout<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<LinesVertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {// position
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {// color
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Unorm8x4,
                }
            ],
        }
    }
}

pub struct LinesInstances {
    pub geometry_idx: usize,
    pub instances: Vec<ObjInstance>,
    pub instance_buffer: wgpu::Buffer,
    pub instance_buffer_dirty: bool,
    pub num_instances: u32
}


impl LinesInstances {
    pub fn new_with_data(scene: &mut Scene,
                         name: &str,
                         geometry_idx: usize,
                         max_instances: u32)
        -> Self
    {
        let renderer = scene.renderer.borrow_mut();
        
        let instances = vec![ObjInstance {model_matrix: Mat4::IDENTITY}; max_instances as usize];
        let instance_data = instances.iter().map(ObjInstance::to_raw).collect::<Vec<_>>();
        let instance_buffer = renderer.gpu.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some(&format!("{:?} instance buffer", name)),
            contents: bytemuck::cast_slice(&instance_data),
            usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
        });
        
        Self {
            geometry_idx: geometry_idx,
            instances: instances,
            instance_buffer: instance_buffer,
            instance_buffer_dirty: false,
            num_instances: 0
        }
    }
}

impl Scene {
    pub fn load_lines(&mut self, name: &str,
                      geometry_idx: usize,
                      max_instances: u32) -> usize
    {
        let instances = LinesInstances::new_with_data(self, name, geometry_idx, max_instances);
        self.lines_instances.push(instances);
        self.lines_instances.len() - 1
    }
    
    pub fn instantiate_lines(&mut self, geometry_idx: usize, instance_info: ObjInstance)
        -> InstanceHandle
    {
        let geom = &mut self.lines_instances[geometry_idx];
        
        geom.instances[geom.num_instances as usize] = instance_info;
        geom.num_instances += 1;
        geom.instance_buffer_dirty = true;
        
        self.lines_ids.push(InstanceID{
            geometry_idx: geometry_idx as u32,
            instance_idx: (geom.num_instances - 1) as u32});
        InstanceHandle{handle_idx: (self.lines_ids.len() - 1) as u32}
    }
    
    pub fn update_lines_instance(&mut self, instance: InstanceHandle,
                                       instance_info: ObjInstance)
    {
        let InstanceID {geometry_idx, instance_idx} = self.lines_ids[instance.handle_idx as usize];
        let geom = &mut self.lines_instances[geometry_idx as usize];
        geom.instances[instance_idx as usize] = instance_info;
        geom.instance_buffer_dirty = true;
    }
}
