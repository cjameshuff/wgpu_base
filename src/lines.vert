#version 450

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec4 a_color;

// Skip slots 2-4 to allow common layout with trimesh shaders
layout(location = 5) in vec4 model_matrix_0;
layout(location = 6) in vec4 model_matrix_1;
layout(location = 7) in vec4 model_matrix_2;
layout(location = 8) in vec4 model_matrix_3;


layout(location = 0) out vec4 v_color;

layout(set = 0, binding = 0)
uniform Uniforms {
    vec3 u_view_position;
    mat4 u_view_proj;
};

void main() {
    mat4 model_matrix = mat4(
        model_matrix_0,
        model_matrix_1,
        model_matrix_2,
        model_matrix_3
    );
    
    vec4 model_space = model_matrix * vec4(a_position, 1.0);
    
    gl_Position = u_view_proj * model_space;
    
    v_color = a_color;
}