
// use std::rc::Rc;
// use std::cell::RefCell;
use std::{num::NonZeroU32, path::Path};

use anyhow::*;
use image::GenericImageView;

use crate::wgpu_renderer::Renderer;


pub struct Texture {
    pub texture: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub sampler: wgpu::Sampler,
    pub bind_group: wgpu::BindGroup
}

impl Texture {
    pub fn load<P: AsRef<Path>>(renderer: &Renderer, path: P, is_normal_map: bool) -> Result<Self> {
        let path_copy = path.as_ref().to_path_buf();
        let label = path_copy.to_str().unwrap();
        let img = image::open(path)?;
        Self::from_image(renderer, &img, label, is_normal_map)
    }
    
    #[allow(dead_code)]
    pub fn from_bytes(renderer: &Renderer, bytes: &[u8], label: &str, is_normal_map: bool)
        -> Result<Self>
    {
        let img = image::load_from_memory(bytes)?;
        Self::from_image(renderer, &img, label, is_normal_map)
    }

    pub fn from_image(renderer: &Renderer, img: &image::DynamicImage, label: &str, is_normal_map: bool)
        -> Result<Self>
    {
        let dimensions = img.dimensions();
        let rgba = img.to_rgba8();
        
        let size = wgpu::Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth_or_array_layers: 1,
        };
        let texture = renderer.gpu.device.create_texture(&wgpu::TextureDescriptor {
            label: Some(label),
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: if is_normal_map {
                wgpu::TextureFormat::Rgba8Unorm
            } else {
                wgpu::TextureFormat::Rgba8UnormSrgb
            },
            usage: wgpu::TextureUsages::TEXTURE_BINDING  | wgpu::TextureUsages::COPY_DST,
        });
        
        renderer.gpu.queue.write_texture(
            wgpu::ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                texture: &texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            &rgba,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: NonZeroU32::new(4 * dimensions.0),
                rows_per_image: NonZeroU32::new(dimensions.1),
            },
            size,
        );
        
        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        
        let sampler = renderer.gpu.device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });
        
        let bind_group = renderer.gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &renderer.layouts.texture_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&sampler),
                }
            ],
            label: Some(label),
        });
        
        Ok(Self {
            texture: texture,
            view: view,
            sampler: sampler,
            bind_group: bind_group
        })
    }
}


pub struct Material {
    pub name: String,
    pub diffuse_texture: Texture,
    pub normal_texture: Texture,
    pub bind_group: wgpu::BindGroup
}

impl Material {
    pub fn new(renderer: &Renderer, name: &str, diffuse_texture: Texture, normal_texture: Texture)
        -> Self
    {
        let bind_group = renderer.gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &renderer.layouts.material_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::TextureView(&normal_texture.view),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: wgpu::BindingResource::Sampler(&normal_texture.sampler),
                },
            ],
            label: Some(name),
        });
        
        Self {
            name: String::from(name),
            diffuse_texture: diffuse_texture,
            normal_texture: normal_texture,
            bind_group: bind_group
        }
    }
    
    // pub fn bind<'a>(&'a self, render_pass: &'a mut wgpu::RenderPass<'a>) {
    //     render_pass.set_bind_group(0, &self.bind_group, &[]);
    // }
}

