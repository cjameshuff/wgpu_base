
use std::path::Path;
use std::rc::Rc;
use std::cell::RefCell;
use std::iter;

use bytemuck::Pod;
use glam::{Mat4};

use wgpu::util::DeviceExt;

use crate::wgpu_renderer::Renderer;
use crate::wgpu_renderer::IndexedGeom;
use crate::wgpu_renderer::VBData;
use crate::wgpu_camera::{Camera, Projection};
use crate::wgpu_texture::Material;
use crate::wgpu_texture::Texture;

use crate::wgpu_trimesh_vc::TrimeshVCInstances;
use crate::wgpu_trimesh_tex::TrimeshTexInstances;
use crate::wgpu_trimesh_mat::TrimeshMatInstances;
use crate::wgpu_lines::LinesInstances;

use crate::colors::*;



#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Uniforms {
    view_position: [f32; 4],
    view_proj: [[f32; 4]; 4],
}

impl Uniforms {
    fn new() -> Self {
        Self {
            view_position: [0.0; 4],
            view_proj: Mat4::IDENTITY.to_cols_array_2d()
        }
    }
    
    fn update_view_proj(&mut self, camera: &Camera, projection: &Projection) {
        self.view_position = camera.pos.extend(1.0).into();
        self.view_proj = (projection.calc_matrix()*camera.calc_matrix()).to_cols_array_2d()
    }
}


#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Light {
    pub position: [f32; 3],
    _padding: u32,
    pub color: [f32; 3],
    _padding2: u32
}


#[derive(Debug, Copy, Clone)]
pub struct ObjInstance {
    pub model_matrix: Mat4
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct ObjInstanceRaw {
    #[allow(dead_code)]
    model_matrix: [[f32; 4]; 4],
}

impl ObjInstance {
    pub fn to_raw(&self) -> ObjInstanceRaw {
        ObjInstanceRaw {model_matrix: self.model_matrix.to_cols_array_2d()}
    }
}

pub struct InstanceID {
    pub geometry_idx: u32,
    pub instance_idx: u32
}

pub struct InstanceHandle {
    pub handle_idx: u32
}

pub struct Scene {
    pub renderer: Rc<RefCell<Renderer>>,
    pub bgcolor: ColorRGB,
    pub camera: Camera,
    pub projection: Projection,
    pub uniforms: Uniforms,
    pub uniform_buffer: wgpu::Buffer,
    pub uniform_bind_group: wgpu::BindGroup,
    pub lights: [Light; 5],
    pub light_buffer: wgpu::Buffer,
    pub light_bind_group: wgpu::BindGroup,
    pub lines_instances: Vec<LinesInstances>,
    pub lines_ids: Vec<InstanceID>,
    pub trimesh_vc_instances: Vec<TrimeshVCInstances>,
    pub trimesh_vc_ids: Vec<InstanceID>,
    pub trimesh_tex_instances: Vec<TrimeshTexInstances>,
    pub trimesh_tex_ids: Vec<InstanceID>,
    pub trimesh_mat_instances: Vec<TrimeshMatInstances>,
    pub trimesh_mat_ids: Vec<InstanceID>
}

impl Scene {
    pub fn new(renderer: Rc<RefCell<Renderer>>, camera: Camera, projection: Projection) -> Self {
        
        let mut uniforms = Uniforms::new();
        uniforms.update_view_proj(&camera, &projection);
        
        let (uniform_buffer, uniform_bind_group) = {
            let renderer = renderer.borrow_mut();
            
            let uniform_buffer = renderer.gpu.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Uniform Buffer"),
                contents: bytemuck::cast_slice(&[uniforms]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });
            
            let uniform_bind_group = renderer.gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &renderer.layouts.uniform_bind_group_layout,
                entries: &[wgpu::BindGroupEntry {
                    binding: 0,
                    resource: uniform_buffer.as_entire_binding(),
                }],
                label: Some("uniform_bind_group"),
            });
            
            (uniform_buffer, uniform_bind_group)
        };
        
        // Lights
        let (lights, light_buffer, light_bind_group) = {
            let lights = [
                Light {
                    position: [0.0, 0.0, 0.0],
                    _padding: 0,
                    color: [0.0, 0.0, 0.0],
                    _padding2: 0
                },
                Light {
                    position: [0.0, 0.0, 0.0],
                    _padding: 0,
                    color: [0.0, 0.0, 0.0],
                    _padding2: 0
                },
                Light {
                    position: [0.0, 0.0, 0.0],
                    _padding: 0,
                    color: [0.0, 0.0, 0.0],
                    _padding2: 0
                },
                Light {
                    position: [0.0, 0.0, 0.0],
                    _padding: 0,
                    color: [0.0, 0.0, 0.0],
                    _padding2: 0
                },
                Light {
                    position: [0.0, 0.0, 0.0],
                    _padding: 0,
                    color: [0.0, 0.0, 0.0],
                    _padding2: 0
                }
            ];
            
            let renderer = renderer.borrow_mut();
            
            let light_buffer = renderer.gpu.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Light VB"),
                contents: bytemuck::cast_slice(&lights),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });
            
            let light_bind_group = renderer.gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &renderer.layouts.lights_bind_group_layout,
                entries: &[wgpu::BindGroupEntry {
                    binding: 0,
                    resource: light_buffer.as_entire_binding(),
                }],
                label: None,
            });
            
            (lights, light_buffer, light_bind_group)
        };
        
        Self {
            renderer: renderer,
            bgcolor: col_rgb(0.0, 0.0, 0.0),
            camera: camera,
            projection: projection,
            uniforms: uniforms,
            uniform_buffer: uniform_buffer,
            uniform_bind_group: uniform_bind_group,
            lights: lights,
            light_buffer: light_buffer,
            light_bind_group: light_bind_group,
            lines_instances: Vec::new(),
            lines_ids: Vec::new(),
            trimesh_vc_instances: Vec::new(),
            trimesh_vc_ids: Vec::new(),
            trimesh_tex_instances: Vec::new(),
            trimesh_tex_ids: Vec::new(),
            trimesh_mat_instances: Vec::new(),
            trimesh_mat_ids: Vec::new()
        }
    }
    
    pub fn reload_instances(&mut self) {
        let renderer = self.renderer.borrow_mut();
        
        for lines in &mut self.lines_instances {
            if lines.instance_buffer_dirty
            {
                lines.instance_buffer_dirty = false;
                let instance_data = lines.instances.iter().map(ObjInstance::to_raw).collect::<Vec<_>>();
                renderer.gpu.queue.write_buffer(
                    &lines.instance_buffer,
                    0,
                    bytemuck::cast_slice(&instance_data),
                );
            }
        }
        
        for trimesh in &mut self.trimesh_vc_instances {
            if trimesh.instance_buffer_dirty
            {
                trimesh.instance_buffer_dirty = false;
                let instance_data = trimesh.instances.iter().map(ObjInstance::to_raw).collect::<Vec<_>>();
                renderer.gpu.queue.write_buffer(
                    &trimesh.instance_buffer,
                    0,
                    bytemuck::cast_slice(&instance_data),
                );
            }
        }
        
        for trimesh in &mut self.trimesh_tex_instances {
            if trimesh.instance_buffer_dirty
            {
                trimesh.instance_buffer_dirty = false;
                let instance_data = trimesh.instances.iter().map(ObjInstance::to_raw).collect::<Vec<_>>();
                renderer.gpu.queue.write_buffer(
                    &trimesh.instance_buffer,
                    0,
                    bytemuck::cast_slice(&instance_data),
                );
            }
        }
        
        for trimesh in &mut self.trimesh_mat_instances {
            if trimesh.instance_buffer_dirty
            {
                trimesh.instance_buffer_dirty = false;
                let instance_data = trimesh.instances.iter().map(ObjInstance::to_raw).collect::<Vec<_>>();
                renderer.gpu.queue.write_buffer(
                    &trimesh.instance_buffer,
                    0,
                    bytemuck::cast_slice(&instance_data),
                );
            }
        }
    }
    
    pub fn reload_lights(&mut self) {
        let renderer = self.renderer.borrow_mut();
        renderer.gpu.queue.write_buffer(&self.light_buffer, 0, bytemuck::cast_slice(&self.lights));
    }
    
    fn update_view_proj(&mut self) {
        let renderer = self.renderer.borrow_mut();
        self.projection.resize(renderer.gpu.size.width, renderer.gpu.size.height);
        self.uniforms.update_view_proj(&self.camera, &self.projection);
        
        renderer.gpu.queue.write_buffer(
            &self.uniform_buffer,
            0,
            bytemuck::cast_slice(&[self.uniforms]),
        );
    }
    
    pub fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        self.reload_instances();
        
        self.update_view_proj();
        
        let renderer = self.renderer.borrow_mut();
        let output = renderer.gpu.surface.get_current_texture()?;
        let view = output.texture.create_view(&wgpu::TextureViewDescriptor::default());
        
        let mut encoder = renderer.gpu.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });
        
        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: self.bgcolor.x as f64,
                            g: self.bgcolor.y as f64,
                            b: self.bgcolor.z as f64,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                    view: &renderer.depth_texture_view,
                    depth_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });
            
            render_pass.set_pipeline(&renderer.pipelines.lines_render_pipeline);
            render_pass.set_bind_group(0, &self.uniform_bind_group, &[]);
            for lines in &self.lines_instances {
                let geom = &renderer.geometry[lines.geometry_idx];
                render_pass.set_vertex_buffer(0, geom.vertex_buffer.slice(..));
                render_pass.set_vertex_buffer(1, lines.instance_buffer.slice(..));
                render_pass.set_index_buffer(geom.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
                
                render_pass.draw_indexed(0..geom.num_indices as u32,
                                         0,
                                         0..lines.num_instances as u32);
            }
            
            render_pass.set_pipeline(&renderer.pipelines.trimesh_vc_render_pipeline);
            render_pass.set_bind_group(0, &self.uniform_bind_group, &[]);
            render_pass.set_bind_group(1, &self.light_bind_group, &[]);
            for trimesh in &self.trimesh_vc_instances {
                let geom = &renderer.geometry[trimesh.geometry_idx];
                render_pass.set_vertex_buffer(0, geom.vertex_buffer.slice(..));
                render_pass.set_vertex_buffer(1, trimesh.instance_buffer.slice(..));
                render_pass.set_index_buffer(geom.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
                
                render_pass.draw_indexed(0..geom.num_indices as u32,
                                         0,
                                         0..trimesh.num_instances as u32);
            }
            
            render_pass.set_pipeline(&renderer.pipelines.trimesh_tex_render_pipeline);
            render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
            render_pass.set_bind_group(2, &self.light_bind_group, &[]);
            for trimesh in &self.trimesh_tex_instances {
                let geom = &renderer.geometry[trimesh.geometry_idx];
                render_pass.set_vertex_buffer(0, geom.vertex_buffer.slice(..));
                render_pass.set_vertex_buffer(1, trimesh.instance_buffer.slice(..));
                render_pass.set_index_buffer(geom.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
                
                render_pass.set_bind_group(0, &renderer.textures[trimesh.texture].bind_group, &[]);
                
                render_pass.draw_indexed(0..geom.num_indices as u32,
                                         0,
                                         0..trimesh.num_instances as u32);
            }
            
            render_pass.set_pipeline(&renderer.pipelines.trimesh_mat_render_pipeline);
            render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
            render_pass.set_bind_group(2, &self.light_bind_group, &[]);
            for trimesh in &self.trimesh_mat_instances {
                let geom = &renderer.geometry[trimesh.geometry_idx];
                render_pass.set_vertex_buffer(0, geom.vertex_buffer.slice(..));
                render_pass.set_vertex_buffer(1, trimesh.instance_buffer.slice(..));
                render_pass.set_index_buffer(geom.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
                
                render_pass.set_bind_group(0, &renderer.materials[trimesh.material].bind_group, &[]);
                
                render_pass.draw_indexed(0..geom.num_indices as u32,
                                         0,
                                         0..trimesh.num_instances as u32);
            }
        }
        renderer.gpu.queue.submit(iter::once(encoder.finish()));
        output.present();
        
        Ok(())
    }
}

impl VBData for ObjInstance {
    fn layout<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<ObjInstance>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Instance,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    // While our vertex shader only uses locations 0, and 1 now, in later tutorials we'll
                    // be using 2, 3, and 4, for Vertex. We'll start at slot 5 not conflict with them later
                    shader_location: 5,
                    format: wgpu::VertexFormat::Float32x4,
                },
                // A mat4 takes up 4 vertex slots as it is technically 4 vec4s. We need to define a slot
                // for each vec4. We don't have to do this in code though.
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
                    shader_location: 6,
                    format: wgpu::VertexFormat::Float32x4,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 8]>() as wgpu::BufferAddress,
                    shader_location: 7,
                    format: wgpu::VertexFormat::Float32x4,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 12]>() as wgpu::BufferAddress,
                    shader_location: 8,
                    format: wgpu::VertexFormat::Float32x4,
                },
            ],
        }
    }
}


impl Renderer {
    pub fn load_geometry<VertT>(&mut self, name: &str, vertices: Vec<VertT>, indices: Vec<u32>)
        -> usize
        where VertT: Pod
    {
        let geom = IndexedGeom::new(self, name, vertices, indices);
        self.geometry.push(geom);
        self.geometry.len() - 1
    }
    
    pub fn load_texture<P: AsRef<Path>>(&mut self, _name: &str, diffuse_path: P)
        -> usize
    {
        self.textures.push(Texture::load(self, diffuse_path, false).unwrap());
        self.textures.len() - 1
    }
    
    pub fn load_material<P: AsRef<Path>>(&mut self, name: &str, diffuse_path: P, normal_path: P)
        -> usize
    {
        let diffuse_texture = Texture::load(self, diffuse_path, false).unwrap();
        let normal_texture = Texture::load(self, normal_path, true).unwrap();
        let material = Material::new(
            self,
            name,
            diffuse_texture,
            normal_texture
        );
        self.materials.push(material);
        self.materials.len() - 1
    }
}
