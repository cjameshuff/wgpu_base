// Triangle mesh with full position+UV+normal+bitangent vertex data.

use wgpu::util::DeviceExt;
use glam::{Vec2, Vec3, Mat4};

use crate::wgpu_scene::Scene;
use crate::wgpu_renderer::VBData;
use crate::wgpu_scene::InstanceHandle;
use crate::wgpu_scene::InstanceID;
use crate::wgpu_scene::ObjInstance;


#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct TrimeshMatVertex {
    pub position: [f32; 3],
    pub tex_coords: [f32; 2],
    pub normal: [f32; 3],
    pub tangent: [f32; 3],
    pub bitangent: [f32; 3]
}

impl VBData for TrimeshMatVertex {
    fn layout<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<TrimeshMatVertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {// position
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x2,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 5]>() as wgpu::BufferAddress,
                    shader_location: 2,
                    format: wgpu::VertexFormat::Float32x3,
                },
                // Tangent and bitangent
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 8]>() as wgpu::BufferAddress,
                    shader_location: 3,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 11]>() as wgpu::BufferAddress,
                    shader_location: 4,
                    format: wgpu::VertexFormat::Float32x3,
                },
            ],
        }
    }
}

pub struct TrimeshMatInstances {
    pub geometry_idx: usize,
    pub instances: Vec<ObjInstance>,
    pub instance_buffer: wgpu::Buffer,
    pub instance_buffer_dirty: bool,
    pub material: usize,
    pub num_instances: u32
}


impl TrimeshMatInstances {
    pub fn new_with_data(scene: &mut Scene,
                         name: &str,
                         geometry_idx: usize,
                         material: usize,
                         max_instances: u32)
        -> Self
    {
        let renderer = scene.renderer.borrow_mut();
        
        let instances = vec![ObjInstance {model_matrix: Mat4::IDENTITY}; max_instances as usize];
        let instance_data = instances.iter().map(ObjInstance::to_raw).collect::<Vec<_>>();
        let instance_buffer = renderer.gpu.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some(&format!("{:?} instance buffer", name)),
            contents: bytemuck::cast_slice(&instance_data),
            usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
        });
        
        Self {
            geometry_idx: geometry_idx,
            instances: instances,
            instance_buffer: instance_buffer,
            instance_buffer_dirty: false,
            material: material,
            num_instances: 0
        }
    }
}

impl Scene {
    pub fn load_trimesh_mat(&mut self, name: &str,
                            geometry_idx: usize,
                            material: usize,
                            max_instances: u32) -> usize
    {
        let instances = TrimeshMatInstances::new_with_data(self, name, geometry_idx, material, max_instances);
        self.trimesh_mat_instances.push(instances);
        self.trimesh_mat_instances.len() - 1
    }
    
    pub fn instantiate_trimesh_mat(&mut self, geometry_idx: usize, instance_info: ObjInstance)
        -> InstanceHandle
    {
        let geom = &mut self.trimesh_mat_instances[geometry_idx];
        
        geom.instances[geom.num_instances as usize] = instance_info;
        geom.num_instances += 1;
        geom.instance_buffer_dirty = true;
        
        self.trimesh_mat_ids.push(InstanceID{
            geometry_idx: geometry_idx as u32,
            instance_idx: (geom.num_instances - 1) as u32});
        InstanceHandle{handle_idx: (self.trimesh_mat_ids.len() - 1) as u32}
    }
    
    pub fn update_trimesh_mat_instance(&mut self, instance: InstanceHandle,
                                       instance_info: ObjInstance)
    {
        let InstanceID {geometry_idx, instance_idx} = self.trimesh_mat_ids[instance.handle_idx as usize];
        let geom = &mut self.trimesh_mat_instances[geometry_idx as usize];
        geom.instances[instance_idx as usize] = instance_info;
        geom.instance_buffer_dirty = true;
    }
}

pub fn compute_triangle_tangents(vertices: &mut [TrimeshMatVertex], indices: &[u32]) {
    // Calculate tangents and bitangents.
    // This code was adapted from the Learn Wgpu example code, and the original code had some
    // problems. In particular, it computed the tangent/bitangent without accounting for vertices
    // being shared between triangles. This should be looked into further to make sure this is
    // correct. 
    // For each triangle:
    for c in indices.chunks(3) {
        let v0 = vertices[c[0] as usize];
        let v1 = vertices[c[1] as usize];
        let v2 = vertices[c[2] as usize];
        
        let pos0: Vec3 = v0.position.into();
        let pos1: Vec3 = v1.position.into();
        let pos2: Vec3 = v2.position.into();
        
        let delta_pos1 = pos1 - pos0;
        let delta_pos2 = pos2 - pos0;
        
        let uv0: Vec2 = v0.tex_coords.into();
        let uv1: Vec2 = v1.tex_coords.into();
        let uv2: Vec2 = v2.tex_coords.into();
        
        let delta_uv1 = uv1 - uv0;
        let delta_uv2 = uv2 - uv0;
        
        let r = 1.0/(delta_uv1.x*delta_uv2.y - delta_uv1.y*delta_uv2.x);
        let tangent = (delta_pos1*delta_uv2.y - delta_pos2*delta_uv1.y)*r;
        let bitangent = (delta_pos2*delta_uv1.x - delta_pos1*delta_uv2.x)*r;
        
        // Add this triangle's contribution to the bitangent of each of its vertices.
        vertices[c[0] as usize].tangent = (Vec3::from_slice(&v0.tangent) + tangent).into();
        vertices[c[1] as usize].tangent = (Vec3::from_slice(&v1.tangent) + tangent).into();
        vertices[c[2] as usize].tangent = (Vec3::from_slice(&v2.tangent) + tangent).into();
        
        vertices[c[0] as usize].bitangent = (Vec3::from_slice(&v0.bitangent) + bitangent).into();
        vertices[c[1] as usize].bitangent = (Vec3::from_slice(&v1.bitangent) + bitangent).into();
        vertices[c[2] as usize].bitangent = (Vec3::from_slice(&v2.bitangent) + bitangent).into();
    }
    
    // Some vertices are shared by multiple triangles and have accumulated multiple bitangents,
    // renormalize these.
    for v in vertices {
        v.tangent = Vec3::from_slice(&v.tangent).normalize().into();
        v.bitangent = Vec3::from_slice(&v.bitangent).normalize().into();
    }
}
