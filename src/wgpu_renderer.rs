
use winit::window::Window;
use wgpu::util::DeviceExt;
use bytemuck::Pod;

use crate::wgpu_texture::{Texture, Material};
use crate::wgpu_scene::{*};

use crate::wgpu_trimesh_vc::TrimeshVCVertex;
use crate::wgpu_trimesh_tex::TrimeshTexVertex;
use crate::wgpu_trimesh_mat::TrimeshMatVertex;
use crate::wgpu_lines::LinesVertex;


pub trait VBData {
    fn layout<'a>() -> wgpu::VertexBufferLayout<'a>;
}

pub struct GPU {
    pub size: winit::dpi::PhysicalSize<u32>,
    pub surface: wgpu::Surface,
    pub device: wgpu::Device,
    pub queue: wgpu::Queue,
    pub surface_config: wgpu::SurfaceConfiguration,
}

impl GPU {
    async fn new(window: &Window) -> Self {
        let size = window.inner_size();
        
        let instance = wgpu::Instance::new(wgpu::Backends::all());
        let surface = unsafe { instance.create_surface(window) };
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::default(),
                },
                None, // Trace path
            )
            .await
            .unwrap();
        
        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface.get_preferred_format(&adapter).unwrap(),
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        surface.configure(&device, &config);
        
        Self {
            surface: surface,
            device: device,
            queue: queue,
            surface_config: config,
            size: size
        }
    }
    
    pub fn create_depth_texture(&self)
        -> (wgpu::Texture, wgpu::TextureView)
    {
        let size = wgpu::Extent3d {
            width: self.size.width,
            height: self.size.height,
            depth_or_array_layers: 1,
        };
        let desc = wgpu::TextureDescriptor {
            label: Some("depth"),
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Depth32Float,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
        };
        let texture = self.device.create_texture(&desc);
        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        
        (texture, view)
    }
}

fn create_trimesh_render_pipeline(device: &wgpu::Device,
                                  layout: &wgpu::PipelineLayout,
                                  color_format: wgpu::TextureFormat,
                                  depth_format: Option<wgpu::TextureFormat>,
                                  vertex_layouts: &[wgpu::VertexBufferLayout],
                                  vs_src: wgpu::ShaderModuleDescriptor,
                                  fs_src: wgpu::ShaderModuleDescriptor)
    -> wgpu::RenderPipeline
{
    let vs_module = device.create_shader_module(&vs_src);
    let fs_module = device.create_shader_module(&fs_src);
    
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Trimesh Pipeline"),
        layout: Some(&layout),
        vertex: wgpu::VertexState {
            module: &vs_module,
            entry_point: "main",
            buffers: vertex_layouts,
        },
        fragment: Some(wgpu::FragmentState {
            module: &fs_module,
            entry_point: "main",
            targets: &[wgpu::ColorTargetState {
                format: color_format,
                blend: Some(wgpu::BlendState {
                    alpha: wgpu::BlendComponent::REPLACE,
                    color: wgpu::BlendComponent::REPLACE,
                }),
                write_mask: wgpu::ColorWrites::ALL,
            }],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: depth_format.map(|format| wgpu::DepthStencilState {
            format,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    })
}

fn create_lines_render_pipeline(device: &wgpu::Device,
                                layout: &wgpu::PipelineLayout,
                                color_format: wgpu::TextureFormat,
                                depth_format: Option<wgpu::TextureFormat>,
                                vertex_layouts: &[wgpu::VertexBufferLayout],
                                vs_src: wgpu::ShaderModuleDescriptor,
                                fs_src: wgpu::ShaderModuleDescriptor)
    -> wgpu::RenderPipeline
{
    let vs_module = device.create_shader_module(&vs_src);
    let fs_module = device.create_shader_module(&fs_src);
    
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Lines Pipeline"),
        layout: Some(&layout),
        vertex: wgpu::VertexState {
            module: &vs_module,
            entry_point: "main",
            buffers: vertex_layouts,
        },
        fragment: Some(wgpu::FragmentState {
            module: &fs_module,
            entry_point: "main",
            targets: &[wgpu::ColorTargetState {
                format: color_format,
                blend: Some(wgpu::BlendState {
                    alpha: wgpu::BlendComponent::REPLACE,
                    color: wgpu::BlendComponent::REPLACE,
                }),
                write_mask: wgpu::ColorWrites::ALL,
            }],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::LineList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: depth_format.map(|format| wgpu::DepthStencilState {
            format,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    })
}

pub struct Pipelines {
    pub trimesh_vc_render_pipeline: wgpu::RenderPipeline,
    pub trimesh_tex_render_pipeline: wgpu::RenderPipeline,
    pub trimesh_mat_render_pipeline: wgpu::RenderPipeline,
    pub lines_render_pipeline: wgpu::RenderPipeline,
}

impl Pipelines {
    fn new(gpu: &GPU, layouts: &Layouts) -> Self {
        let lines_pl_layout = gpu.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Line Pipeline Layout"),
            bind_group_layouts: &[
                &layouts.uniform_bind_group_layout
            ],
            push_constant_ranges: &[],
        });
        
        let lines_render_pipeline = create_lines_render_pipeline(
                &gpu.device,
                &lines_pl_layout,
                gpu.surface_config.format,
                Some(wgpu::TextureFormat::Depth32Float),
                &[LinesVertex::layout(), ObjInstance::layout()],
                wgpu::include_spirv!("lines.vert.spv"),
                wgpu::include_spirv!("lines.frag.spv"),
            );
        
        // Triangle mesh render pipeline
        let trimesh_vc_layout = gpu.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Vertex-colored Pipeline Layout"),
                bind_group_layouts: &[
                    &layouts.uniform_bind_group_layout,
                    &layouts.lights_bind_group_layout,
                ],
                push_constant_ranges: &[],
            });
        
        let trimesh_vc_render_pipeline = create_trimesh_render_pipeline(
            &gpu.device,
            &trimesh_vc_layout,
            gpu.surface_config.format,
            Some(wgpu::TextureFormat::Depth32Float),
            &[TrimeshVCVertex::layout(), ObjInstance::layout()],
            // Workaround for validation error
            // wgpu::ShaderModuleDescriptor {
            //     label: Some("Vertex-colored Vertex Shader"),
            //     source: wgpu::util::make_spirv(include_bytes!("trimesh_vc.vert.spv")),
            //     flags: wgpu::ShaderFlags::default(),
            // },
            // wgpu::ShaderModuleDescriptor {
            //     label: Some("Vertex-colored Fragment Shader"),
            //     source: wgpu::util::make_spirv(include_bytes!("trimesh_vc.frag.spv")),
            //     flags: wgpu::ShaderFlags::default(),
            // }
            wgpu::include_spirv!("trimesh_vc.vert.spv"),
            wgpu::include_spirv!("trimesh_vc.frag.spv"),
        );
        
        // Triangle mesh render pipeline
        let trimesh_tex_pl_layout = gpu.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Textured Pipeline Layout"),
                bind_group_layouts: &[
                    &layouts.texture_bind_group_layout,
                    &layouts.uniform_bind_group_layout,
                    &layouts.lights_bind_group_layout,
                ],
                push_constant_ranges: &[],
            });
        
        let trimesh_tex_render_pipeline = create_trimesh_render_pipeline(
            &gpu.device,
            &trimesh_tex_pl_layout,
            gpu.surface_config.format,
            Some(wgpu::TextureFormat::Depth32Float),
            &[TrimeshTexVertex::layout(), ObjInstance::layout()],
            // Workaround for validation error
            // wgpu::ShaderModuleDescriptor {
            //     label: Some("Textured Vertex Shader"),
            //     source: wgpu::util::make_spirv(include_bytes!("trimesh_tex.vert.spv")),
            //     flags: wgpu::ShaderFlags::default(),
            // },
            // wgpu::ShaderModuleDescriptor {
            //     label: Some("Textured Fragment Shader"),
            //     source: wgpu::util::make_spirv(include_bytes!("trimesh_tex.frag.spv")),
            //     flags: wgpu::ShaderFlags::default(),
            // }
            wgpu::include_spirv!("trimesh_tex.vert.spv"),
            wgpu::include_spirv!("trimesh_tex.frag.spv"),
        );
        
        // Triangle mesh render pipeline
        let trimesh_f_pl_layout = gpu.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Material Pipeline Layout"),
                bind_group_layouts: &[
                    &layouts.material_bind_group_layout,
                    &layouts.uniform_bind_group_layout,
                    &layouts.lights_bind_group_layout,
                ],
                push_constant_ranges: &[],
            });
        
        let trimesh_mat_render_pipeline = create_trimesh_render_pipeline(
            &gpu.device,
            &trimesh_f_pl_layout,
            gpu.surface_config.format,
            Some(wgpu::TextureFormat::Depth32Float),
            &[TrimeshMatVertex::layout(), ObjInstance::layout()],
            // Workaround for validation error
            // wgpu::ShaderModuleDescriptor {
            //     label: Some("Material Vertex Shader"),
            //     source: wgpu::util::make_spirv(include_bytes!("trimesh_mat.vert.spv")),
            //     flags: wgpu::ShaderFlags::default(),
            // },
            // wgpu::ShaderModuleDescriptor {
            //     label: Some("Material Fragment Shader"),
            //     source: wgpu::util::make_spirv(include_bytes!("trimesh_mat.frag.spv")),
            //     flags: wgpu::ShaderFlags::default(),
            // }
            wgpu::include_spirv!("trimesh_mat.vert.spv"),
            wgpu::include_spirv!("trimesh_mat.frag.spv"),
        );
        
        Self {
            trimesh_vc_render_pipeline: trimesh_vc_render_pipeline,
            trimesh_tex_render_pipeline: trimesh_tex_render_pipeline,
            trimesh_mat_render_pipeline: trimesh_mat_render_pipeline,
            lines_render_pipeline: lines_render_pipeline
        }
    }
}

pub struct Layouts {
    pub texture_bind_group_layout: wgpu::BindGroupLayout,
    pub material_bind_group_layout: wgpu::BindGroupLayout,
    pub lights_bind_group_layout: wgpu::BindGroupLayout,
    pub uniform_bind_group_layout: wgpu::BindGroupLayout,
}

impl Layouts {
    fn new(gpu: &GPU) -> Self {
        let texture_bind_group_layout =
            gpu.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    // Diffuse map
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                            view_dimension: wgpu::TextureViewDimension::D2,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    }
                ],
                label: Some("texture_bind_group_layout"),
            });
        
        let material_bind_group_layout =
            gpu.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    // Diffuse map
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                            view_dimension: wgpu::TextureViewDimension::D2,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                    // Normal map
                    wgpu::BindGroupLayoutEntry {
                        binding: 2,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                            view_dimension: wgpu::TextureViewDimension::D2,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 3,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                    // TODO: specular map, emission map
                ],
                label: Some("material_bind_group_layout"),
            });
        
        let lights_bind_group_layout = gpu.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX | wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: None,
            });
        
        let uniform_bind_group_layout = gpu.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX | wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("uniform_bind_group_layout"),
            });
        
        Self {
            texture_bind_group_layout: texture_bind_group_layout,
            material_bind_group_layout: material_bind_group_layout,
            lights_bind_group_layout: lights_bind_group_layout,
            uniform_bind_group_layout: uniform_bind_group_layout
        }
    }
}


pub struct IndexedGeom {
    pub vertex_buffer: wgpu::Buffer,
    pub num_vertices: usize,
    pub index_buffer: wgpu::Buffer,
    pub num_indices: usize
}

impl IndexedGeom {
    pub fn new<VertT>(renderer: &mut Renderer, name: &str, vertices: Vec<VertT>, indices: Vec<u32>)
        -> Self
        where VertT: Pod
    {
        let vertex_buffer = renderer.gpu.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some(&format!("{:?} vertex buffer", name)),
            contents: bytemuck::cast_slice(&vertices),
            usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
        });
        
        let index_buffer = renderer.gpu.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some(&format!("{:?} index buffer", name)),
            contents: bytemuck::cast_slice(&indices),
            usage: wgpu::BufferUsages::INDEX | wgpu::BufferUsages::COPY_DST,
        });
        IndexedGeom {
            vertex_buffer: vertex_buffer,
            num_vertices: vertices.len(),
            index_buffer: index_buffer,
            num_indices: indices.len()
        }
    }
}


pub struct Renderer {
    pub gpu: GPU,
    pub layouts: Layouts,
    pub pipelines: Pipelines,
    pub depth_texture: wgpu::Texture,
    pub depth_texture_view: wgpu::TextureView,
    pub textures: Vec<Texture>,
    pub materials: Vec<Material>,
    pub geometry: Vec<IndexedGeom>
}

impl Renderer {
    pub async fn new(window: &Window) -> Self {
        let gpu = GPU::new(window).await;
        
        let layouts = Layouts::new(&gpu);
        
        let (depth_texture, depth_texture_view) = gpu.create_depth_texture();
        
        let pipelines = Pipelines::new(&gpu, &layouts);
        
        let mut renderer = Self {
            gpu: gpu,
            layouts: layouts,
            pipelines: pipelines,
            depth_texture: depth_texture,
            depth_texture_view: depth_texture_view,
            textures: Vec::new(),
            materials: Vec::new(),
            geometry: Vec::new()
        };
        
        {
            let diffuse_bytes = include_bytes!("../res/cobble-diffuse.png");
            let normal_bytes = include_bytes!("../res/cobble-normal.png");
            
            let diffuse_texture = Texture::from_bytes(
                &renderer,
                diffuse_bytes,
                "res/alt-diffuse.png",
                false,
            )
            .unwrap();
            let normal_texture = Texture::from_bytes(
                &renderer,
                normal_bytes,
                "res/alt-normal.png",
                true,
            )
            .unwrap();
            
            renderer.materials.push(Material::new(
                &renderer,
                "alt-material",
                diffuse_texture,
                normal_texture
            ));
        };
        
        renderer
    }
    
    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.gpu.size = new_size;
        self.gpu.surface_config.width = new_size.width;
        self.gpu.surface_config.height = new_size.height;
        self.gpu.surface.configure(&self.gpu.device, &self.gpu.surface_config);

        let (depth_texture, depth_texture_view) = self.gpu.create_depth_texture();
        self.depth_texture = depth_texture;
        self.depth_texture_view = depth_texture_view;
    }
}
