use glam::{Vec3, vec3, Mat4};

#[derive(Debug)]
pub struct Camera {
    pub sky: Vec3,
    pub pos: Vec3,
    pub dir: Vec3,
    pub right: Vec3,
    pub up: Vec3,
}

impl Camera {
    pub fn new_from_look_at(position: Vec3, look_at: Vec3, sky: Vec3)
        -> Self
    {
        let mut cam = Self {
            pos: position,
            dir: vec3(0.0, 0.0, 0.0),
            right: vec3(0.0, 0.0, 0.0),
            up: vec3(0.0, 0.0, 0.0),
            sky: sky
        };
        cam.look_at(look_at);
        cam
    }
    
    pub fn new_from_pitch_yaw(position: Vec3, pitch: f32, yaw: f32, sky: Vec3)
        -> Self
    {
        let mut cam = Self {
            pos: position,
            dir: vec3(0.0, 0.0, 0.0),
            right: vec3(0.0, 0.0, 0.0),
            up: vec3(0.0, 0.0, 0.0),
            sky: sky
        };
        cam.pitch_yaw(pitch, yaw);
        cam
    }
    
    pub fn look_at(&mut self, look_at: Vec3) {
        self.dir = (look_at - self.pos).normalize();
        self.right = self.dir.cross(self.sky).normalize();
        self.up = self.right.cross(self.dir).normalize();
    }
    
    pub fn pitch_yaw(&mut self, pitch: f32, yaw: f32) {
        self.dir = vec3(f32::cos(yaw), f32::sin(pitch), f32::sin(yaw)).normalize();
        self.right = self.dir.cross(self.sky).normalize();
        self.up = self.right.cross(self.dir).normalize();
    }
    
    pub fn calc_matrix(&self) -> Mat4 {
        let p = self.pos;
        let d = self.dir;
        let r = self.right;
        let u = self.up;
        Mat4::from_cols_array(&[
            r.x, u.x, -d.x, 0.0,
            r.y, u.y, -d.y, 0.0,
            r.z, u.z, -d.z, 0.0,
            -p.dot(r),-p.dot(u), p.dot(d), 1.0
        ])
    }
}

pub struct Projection {
    aspect: f32,
    fovy: f32,
    znear: f32,
    zfar: f32,
}

impl Projection {
    pub fn new(width: u32, height: u32, fovy: f32, znear: f32, zfar: f32) -> Self {
        Self {
            aspect: (width as f32)/(height as f32),
            fovy: fovy,
            znear: znear,
            zfar: zfar,
        }
    }
    
    pub fn resize(&mut self, width: u32, height: u32) {
        self.aspect = (width as f32)/(height as f32);
    }
    
    pub fn calc_matrix(&self) -> Mat4 {
        Mat4::perspective_rh(f32::to_radians(self.fovy), self.aspect, self.znear, self.zfar)
    }
}
