
use glam::{Vec3, vec3, Vec4, vec4};

pub type ColorRGB = Vec3;
pub type ColorRGBA = Vec4;

pub fn col_rgb(r: f32, g: f32, b: f32) -> ColorRGB {
    vec3(r, g, b)
}

pub fn col_rgba(r: f32, g: f32, b: f32, a: f32) -> ColorRGBA {
    vec4(r, g, b, a)
}

fn col_to_u8x3(col: ColorRGB) -> [u8; 3] {
    [
        (col.x*256.0).clamp(0.0, 255.0) as u8,
        (col.y*256.0).clamp(0.0, 255.0) as u8,
        (col.z*256.0).clamp(0.0, 255.0) as u8
    ]
}

fn col_to_u8x4(col: ColorRGBA) -> [u8; 4] {
    [
        (col.x*256.0).clamp(0.0, 255.0) as u8,
        (col.y*256.0).clamp(0.0, 255.0) as u8,
        (col.z*256.0).clamp(0.0, 255.0) as u8,
        (col.w*256.0).clamp(0.0, 255.0) as u8
    ]
}

// pub struct ColorRGB {
//     pub r: f32,
//     pub g: f32,
//     pub b: f32
// }

// pub struct ColorRGBA {
//     pub r: f32,
//     pub g: f32,
//     pub b: f32,
//     pub a: f32
// }

// pub fn col_rgb(r: f32, g: f32, b: f32) -> ColorRGB {
//     ColorRGB {r, g, b}
// }

// pub fn col_rgba(r: f32, g: f32, b: f32, a: f32) -> ColorRGBA {
//     ColorRGBA {r, g, b, a}
// }

// fn col_to_u8x3(col: ColorRGB) -> [u8; 3] {
//     [
//         (col.r*256.0).floor() as u8,
//         (col.g*256.0).floor() as u8,
//         (col.b*256.0).floor() as u8
//     ]
// }