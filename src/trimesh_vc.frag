#version 450

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec4 v_color;
layout(location = 3) in mat4 v_modelview_mat;

layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0)
uniform Uniforms {
    vec3 u_view_position;
    mat4 u_view_proj;
};

struct Light {
    vec3 light_position;
    vec3 light_color;
};

#define NUM_LIGHTS  5
layout(set = 1, binding = 0) uniform Lights {
    Light lights[NUM_LIGHTS];
};

void main() {
    // vec3 view_dir = normalize(u_view_position - v_position);
    
    float spec_pwr = 32.0;
    vec3 color = vec3(0.0, 0.0, 0.0);
    
    for(int i = 0; i < NUM_LIGHTS; ++i) {
        vec3 light_pos = lights[i].light_position;
        // vec3 light_pos = (v_modelview_mat*vec4(lights[i].light_position, 0.0)).xyz;
        vec3 light_dir = normalize(light_pos - v_position);
        
        color += lights[i].light_color*max(dot(v_normal, light_dir), 0.0);
        
        // vec3 half_dir = normalize(view_dir + light_dir);
        // color += lights[i].light_color*pow(max(dot(v_normal, half_dir), 0.0), spec_pwr);
    }
    
    // f_color = v_color;
    f_color = vec4(color, 1.0);
    // f_color = vec4(color*v_color.xyz, v_color.a);
}