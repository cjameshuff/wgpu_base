
mod wgpu_renderer;
mod wgpu_camera;
mod wgpu_texture;
mod wgpu_scene;
mod wgpu_lines;
mod wgpu_trimesh_vc;
mod wgpu_trimesh_tex;
mod wgpu_trimesh_mat;
mod colors;


use std::rc::Rc;
use std::cell::RefCell;
use std::f64::consts::PI;
use std::f64::consts::FRAC_PI_2;
use std::time::Duration;
use std::path::Path;
use std::collections::HashMap;

use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::Window,
    dpi::PhysicalPosition,
    dpi::PhysicalSize
};

use glam::{Vec3, vec3, Mat4, Quat};

use crate::wgpu_camera::Camera;
use crate::wgpu_camera::Projection;
use crate::wgpu_renderer::Renderer;
use crate::wgpu_scene::Scene;
use crate::wgpu_scene::ObjInstance;
use crate::wgpu_trimesh_vc::TrimeshVCVertex;
// use crate::wgpu_trimesh_tex::TrimeshTexVertex;
use crate::wgpu_trimesh_mat::TrimeshMatVertex;
use crate::wgpu_trimesh_mat::compute_triangle_tangents;
use crate::wgpu_lines::LinesVertex;

#[derive(Debug)]
pub enum InputAction {
    MoveForward,
    MoveBackward,
    MoveLeft,
    MoveRight,
    MoveUp,
    MoveDown,
    Nothing
}

#[derive(Debug)]
pub struct InputBinding {
    action: InputAction,
    pressed: bool
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum InputButtonId {
    MouseLeft,
    MouseMiddle,
    MouseRight,
    Mouse4,
    Mouse5,
    ScrollDown,
    ScrollUp
}

#[derive(Debug)]
pub struct InputHandler {
    pitch: f64,
    yaw: f64,
    move_r: i32,
    move_f: i32,
    move_u: i32,
    lmouse_pressed: bool,
    scroll_amt: f64,
    speed: f64,
    mouse_sens: f64,
}

impl InputHandler {
    pub fn new(speed: f64, mouse_sens: f64) -> Self {
        Self {
            pitch: 0.0,
            yaw: -PI*0.5,
            move_r: 0,
            move_f: 0,
            move_u: 0,
            lmouse_pressed: false,
            scroll_amt: 0.0,
            speed,
            mouse_sens,
        }
    }
    
    pub fn do_action(&mut self, binding: &InputBinding) {
        let delta = if binding.pressed {1} else {-1};
        match binding.action {
            InputAction::MoveForward => {
                self.move_f += delta;
            }
            InputAction::MoveBackward => {
                self.move_f -= delta;
            }
            InputAction::MoveLeft => {
                self.move_r -= delta;
            }
            InputAction::MoveRight => {
                self.move_r += delta;
            }
            InputAction::MoveUp => {
                self.move_u += delta;
            }
            InputAction::MoveDown => {
                self.move_u -= delta;
            }
            InputAction::Nothing => {}
        }
        
        println!("f={} r={} u={}",
            self.move_f,
            self.move_r,
            self.move_u);
    }

    pub fn mouse_moved(&mut self, mouse_dx: f64, mouse_dy: f64) {
        if self.lmouse_pressed {
            self.yaw = (self.yaw + mouse_dx*self.mouse_sens).rem_euclid(PI*2.0);
            self.pitch = (self.pitch - mouse_dy*self.mouse_sens).clamp(-FRAC_PI_2, FRAC_PI_2);
        }
    }

    pub fn mouse_button(&mut self, button: ButtonId, state: ElementState) {
        if button == 1
        {
            self.lmouse_pressed = state == ElementState::Pressed;
        }
    }

    pub fn mouse_scroll(&mut self, delta: f64) {
        self.scroll_amt += delta;
    }

    pub fn update(&mut self, camera: &mut Camera, dt: Duration) {
        let dt = dt.as_secs_f32()*(self.speed as f32);
        
        // self.move_* variables are counts of pressed keys controlling each action. We only want to
        // count each action once, but count it if any of its keys are pressed.
        let mf = self.move_f.max(-1).min(1) as f32;
        let mr = self.move_r.max(-1).min(1) as f32;
        let mu = self.move_u.max(-1).min(1) as f32;
        
        camera.pos += camera.dir*(mf*dt);
        camera.pos += camera.right*(mr*dt);
        camera.pos += camera.up*(mu*dt);
        
        camera.pitch_yaw(self.pitch as f32, self.yaw as f32);
        
        self.scroll_amt = 0.0;
    }
}

struct ProgramState {
    renderer: Rc<RefCell<Renderer>>,
    scene: Rc<RefCell<Scene>>,
    key_bindings: HashMap<VirtualKeyCode, InputBinding>,
    button_bindings: HashMap<InputButtonId, InputBinding>,
    input_handler: InputHandler,
    frame_ctr: u32,
}

fn load_mesh<P: AsRef<Path>>(path: P)
    -> (Vec<TrimeshMatVertex>, Vec<u32>)
{
    let (obj_models, _obj_materials) = tobj::load_obj(path.as_ref(), true).unwrap();
    
    let model = &obj_models[0];
    
    let mut vertices = Vec::new();
    for i in 0..model.mesh.positions.len()/3 {
        vertices.push(TrimeshMatVertex {
            position: [
                model.mesh.positions[i*3],
                model.mesh.positions[i*3 + 1],
                model.mesh.positions[i*3 + 2],
            ]
            .into(),
            tex_coords: [model.mesh.texcoords[i * 2], model.mesh.texcoords[i*2 + 1]].into(),
            normal: [
                model.mesh.normals[i*3],
                model.mesh.normals[i*3 + 1],
                model.mesh.normals[i*3 + 2],
            ]
            .into(),
            tangent: [0.0; 3].into(),
            bitangent: [0.0; 3].into(),
        });
    }
    
    let indices = &model.mesh.indices;
    compute_triangle_tangents(&mut vertices, &indices);
    
    (vertices, indices.clone())
}

impl ProgramState {
    async fn new(window: &Window) -> Self {
        let size = window.inner_size();
        
        let camera = Camera::new_from_look_at(vec3(0.0, 5.0, 10.0), vec3(0.0, 5.0, 0.0), vec3(0.0, 1.0, 0.0));
        let projection = Projection::new(size.width, size.height, 45.0, 0.1, 100.0);
        
        let renderer = Rc::new(RefCell::new(Renderer::new(window).await));
        let scene = Rc::new(RefCell::new(Scene::new(renderer.clone(), camera, projection)));
        
        let res_dir = Path::new(env!("OUT_DIR")).join("res");
        
        {
            let mesh_path = res_dir.join("cube.obj");
            let tex_d_path = res_dir.join("cube-diffuse.jpg");
            let tex_n_path = res_dir.join("cube-normal.png");
            
            let material = renderer.borrow_mut().load_material("cube", tex_d_path, tex_n_path);
            
            const NUM_INSTANCES_PER_ROW: u32 = 10;
            const NUM_INSTANCES: u32 = NUM_INSTANCES_PER_ROW*NUM_INSTANCES_PER_ROW;
            const SPACE_BETWEEN: f32 = 3.0;
            
            let now = std::time::Instant::now();
            let (cube_vertices, cube_indices) = load_mesh(mesh_path);
            println!("Elapsed (Original): {:?}", std::time::Instant::now() - now);
            
            let cube_geom = renderer.borrow_mut().load_geometry("cube.obj", cube_vertices, cube_indices);
            let cube_list = scene.borrow_mut().load_trimesh_mat( "cube.obj", cube_geom, material, NUM_INSTANCES);
            
            for z in 0..NUM_INSTANCES_PER_ROW {
                for x in 0..NUM_INSTANCES_PER_ROW {
                    let fx = SPACE_BETWEEN * (x as f32 - NUM_INSTANCES_PER_ROW as f32 / 2.0);
                    let fz = SPACE_BETWEEN * (z as f32 - NUM_INSTANCES_PER_ROW as f32 / 2.0);
                    
                    let position = vec3(fx, 0.0, fz);
                    
                    let rotation = if x == 0 && z == 0 {
                        Quat::from_axis_angle(Vec3::Z, f32::to_radians(0.0))
                    } else {
                        Quat::from_axis_angle(position.normalize(), f32::to_radians(45.0))
                    };
                    
                    scene.borrow_mut().instantiate_trimesh_mat(cube_list, ObjInstance {
                        model_matrix:
                            Mat4::from_translation(position)
                            *Mat4::from_quat(rotation)
                    });
                }
            }
        }
        
        {
            let vertices = vec![
                TrimeshVCVertex{position: [-1.0, 0.0, 0.0], normal: [-1.0, 0.0, 0.0], color: [255, 0, 0, 255]},// A
                TrimeshVCVertex{position: [ 1.0, 0.0, 0.0], normal: [ 1.0, 0.0, 0.0], color: [255, 0, 0, 255]},// B
                TrimeshVCVertex{position: [ 0.0,-1.0, 0.0], normal: [ 0.0,-1.0, 0.0], color: [0, 255, 0, 255]},// C
                TrimeshVCVertex{position: [ 0.0, 1.0, 0.0], normal: [ 0.0, 1.0, 0.0], color: [0, 255, 0, 255]},// D
                TrimeshVCVertex{position: [ 0.0, 0.0,-1.0], normal: [ 0.0, 0.0,-1.0], color: [0, 0, 255, 255]},// E
                TrimeshVCVertex{position: [ 0.0, 0.0, 1.0], normal: [ 0.0, 0.0, 1.0], color: [0, 0, 255, 255]},// F
            ];
            // v3--v0---v3
            //  |\ /|\ /|
            //  |v4 |v5 |
            //  |/ \|/ \|
            // v1--v2---v1
            let indices = vec![
                4, 0, 3,
                4, 3, 1,
                4, 1, 2,
                4, 2, 0,
                5, 0, 2,
                5, 2, 1,
                5, 1, 3,
                5, 3, 0
            ];
            
            let oct_geom = renderer.borrow_mut().load_geometry("octahedron", vertices, indices);
            let oct_list = scene.borrow_mut().load_trimesh_vc("octahedron", oct_geom, 1);
            
            scene.borrow_mut().instantiate_trimesh_vc(oct_list, ObjInstance {
                model_matrix: Mat4::from_translation(vec3(0.0, 5.0, 0.0))
            });
        }
        
        {
            let vertices = vec![
                LinesVertex{position: [-1.0,-1.0,-1.0], color: [255, 0, 0, 255]},
                LinesVertex{position: [ 1.0,-1.0,-1.0], color: [0, 255, 0, 255]},
                LinesVertex{position: [ 1.0, 1.0,-1.0], color: [0, 0, 255, 255]},
                LinesVertex{position: [-1.0, 1.0,-1.0], color: [255, 255, 255, 255]},
                LinesVertex{position: [-1.0,-1.0, 1.0], color: [0, 0, 0, 255]},
                LinesVertex{position: [ 1.0,-1.0, 1.0], color: [0, 255, 255, 255]},
                LinesVertex{position: [ 1.0, 1.0, 1.0], color: [255, 0, 255, 255]},
                LinesVertex{position: [-1.0, 1.0, 1.0], color: [255, 255, 0, 255]},
            ];
            let indices = vec![
                0, 1,
                1, 2,
                2, 3,
                3, 0,
                4, 5,
                5, 6,
                6, 7,
                7, 4,
                0, 4,
                1, 5,
                2, 6,
                3, 7
            ];
            
            let cage_geom = renderer.borrow_mut().load_geometry("cube-cage", vertices, indices);
            let cage_list = scene.borrow_mut().load_lines("cube-cage", cage_geom, 1);
            
            scene.borrow_mut().instantiate_lines(cage_list, ObjInstance {
                model_matrix: Mat4::from_translation(vec3(0.0, 5.0, 0.0))
            });
        }
        
        scene.borrow_mut().lights[0].color = [0.2, 0.0, 0.0];
        scene.borrow_mut().lights[1].position = [2.0, 2.0, 2.0];
        scene.borrow_mut().lights[1].color = [1.0, 1.0, 1.0];
        
        let mut key_bindings = HashMap::new();
        key_bindings.insert(VirtualKeyCode::W, InputBinding{action: InputAction::MoveForward, pressed: false});
        key_bindings.insert(VirtualKeyCode::Up, InputBinding{action: InputAction::MoveForward, pressed: false});
        key_bindings.insert(VirtualKeyCode::S, InputBinding{action: InputAction::MoveBackward, pressed: false});
        key_bindings.insert(VirtualKeyCode::Down, InputBinding{action: InputAction::MoveBackward, pressed: false});
        key_bindings.insert(VirtualKeyCode::A, InputBinding{action: InputAction::MoveLeft, pressed: false});
        key_bindings.insert(VirtualKeyCode::Left, InputBinding{action: InputAction::MoveLeft, pressed: false});
        key_bindings.insert(VirtualKeyCode::D, InputBinding{action: InputAction::MoveRight, pressed: false});
        key_bindings.insert(VirtualKeyCode::Right, InputBinding{action: InputAction::MoveRight, pressed: false});
        key_bindings.insert(VirtualKeyCode::Q, InputBinding{action: InputAction::MoveUp, pressed: false});
        key_bindings.insert(VirtualKeyCode::Z, InputBinding{action: InputAction::MoveDown, pressed: false});
        
        let mut button_bindings = HashMap::new();
        button_bindings.insert(InputButtonId::MouseLeft, InputBinding{action: InputAction::Nothing, pressed: false});
        
        Self {
            renderer: renderer,
            scene: scene,
            key_bindings: key_bindings,
            button_bindings: button_bindings,
            input_handler: InputHandler::new(4.0, 1.0/200.0),
            frame_ctr: 0
        }
    }
    
    fn update(&mut self, dt: std::time::Duration) {
        self.input_handler.update(&mut self.scene.borrow_mut().camera, dt);
        
        let cam_pos = self.scene.borrow_mut().camera.pos;
        
        self.scene.borrow_mut().lights[0].position = cam_pos.into();
        
        let old_position: Vec3 = self.scene.borrow().lights[1].position.into();
        self.scene.borrow_mut().lights[1].position =
            (Quat::from_axis_angle(vec3(0.0, 1.0, 0.0), f32::to_radians(1.0))*old_position)
                .into();
        
        self.scene.borrow_mut().reload_lights();
    }
    
    fn input(&mut self, event: &DeviceEvent) -> bool {
        match event {
            DeviceEvent::Key(KeyboardInput {virtual_keycode: Some(key), state, ..}) => {
                // TODO: Use HashMap::entry() instead?
                match self.key_bindings.get_mut(&key) {
                    Some(binding) => {
                        if *state == ElementState::Pressed && !binding.pressed {
                            binding.pressed = true;
                            self.input_handler.do_action(binding);
                        }
                        else if *state != ElementState::Pressed && binding.pressed {
                            binding.pressed = false;
                            self.input_handler.do_action(binding);
                        }
                        true
                    }
                    _ => false
                }
            }
            DeviceEvent::MouseWheel {delta, ..} => {
                let delta_f = match delta {
                    MouseScrollDelta::LineDelta(_, scroll_amt) =>
                        -(*scroll_amt as f64)*0.5,
                    MouseScrollDelta::PixelDelta(PhysicalPosition { y: scroll_amt, .. }) =>
                        -*scroll_amt,
                };
                self.input_handler.mouse_scroll(delta_f);
                true
            }
            DeviceEvent::Button {button, state} => {
                self.input_handler.mouse_button(*button, *state);
                true
            }
            DeviceEvent::MouseMotion {delta} => {
                self.input_handler.mouse_moved(delta.0, delta.1);
                true
            }
            _ => false,
        }
    }

    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.renderer.borrow_mut().resize(new_size);
    }
    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        self.frame_ctr += 1;
        self.scene.borrow_mut().render()
    }
}

fn main() {
    env_logger::init();
    let event_loop = EventLoop::new();
    let title = env!("CARGO_PKG_NAME");
    let size = PhysicalSize::new(1600, 1200);
    
    let window = winit::window::WindowBuilder::new()
        .with_title(title)
        .with_inner_size(size)
        .with_min_inner_size(size)
        .build(&event_loop)
        .unwrap();
    
    use futures::executor::block_on;
    let mut state = block_on(ProgramState::new(&window));
    
    let mut last_render_time = std::time::Instant::now();
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            Event::MainEventsCleared => window.request_redraw(),
            Event::DeviceEvent {
                ref event,
                .. // We're not using device_id currently
            } => {
                state.input(event);
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == window.id() => {
                match event {
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    WindowEvent::KeyboardInput { input, .. } => match input {
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        } => {
                            *control_flow = ControlFlow::Exit;
                        }
                        _ => {}
                    },
                    WindowEvent::Resized(physical_size) => {
                        state.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        state.resize(**new_inner_size);
                    }
                    _ => {}
                }
            }
            Event::RedrawRequested(_) => {
                let now = std::time::Instant::now();
                let dt = now - last_render_time;
                last_render_time = now;
                state.update(dt);
                match state.render() {
                    Ok(_) => {}
                    // Reconfigure the surface if it's lost or outdated
                    Err(wgpu::SurfaceError::Lost | wgpu::SurfaceError::Outdated) => {
                        let size = state.renderer.borrow().gpu.size;
                        state.resize(size)
                    }
                    // The system is out of memory, we should probably quit
                    Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                    // All other errors (Outdated, Timeout) should be resolved by the next frame
                    Err(e) => eprintln!("{:?}", e),
                }
            }
            _ => {}
        }
    });
}
